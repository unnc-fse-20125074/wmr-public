# README 

### Project Name: Webpages Monitor Robot 

### Develop Team: Team 16



## Contributors

- **Xi YU**
- **Jiayin WANG**
- **Zhekai HAN**
- **Zibo ZHAO**
- **Zihan LIU**



## Environment Requirements

**Required OS:**

- ​	Windows

- ​	macOS(still in the development stage)




## Installation Instruction

Download source code and unzip file according to the picture. 

- [Download Demonstration](img/Download.jpeg)

After download, open wmr-public/Windows/dist/main_window.(wmr-public/macOS(alpha)/dist/main_window for macOS users)

Execute main_window.



## User Manual

- [User Manual](User Manual/User Manual.pdf)



## Others

The first execution may be slow, please be patient.

Please ensure a good internet connection.

If you have any problems or cooperation intention, please contact us:

​	[scyxy1@nottingham.edu.cn](scyxy1@nottingham.edu.cn)

​	[grpteam16@163.com](grpteam16@163.com)

